import React from "react";
import { hot } from 'react-hot-loader/root';
import Header from './Components/Header/Header'
import Input from './Components/Input/Input'


class App extends React.Component {    
   render() {   
      return (
            <div>
               <Header />
               <Input />
            </div>
      );
   }
}
export default hot(App);