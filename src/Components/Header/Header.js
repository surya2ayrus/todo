import React, { Component } from 'react'
import '../../../css/header.css'

class Header extends Component {
    render() {
        return (
            <div className="container">
                <h2 className="display-4 jumbotron custom-header"> Tasks </h2>
            </div>
        )
    }
}

export default Header
