import React, { Component, createRef } from 'react'
import '../../../css/input.css'
import TodoPage from '../TodoPage/TodoPage';
import CompletedTodoPage from '../TodoPage/CompletedTodoPage'
import {connect} from 'react-redux';
import {ADDTODOTASK,ADDCOMPLETEDTASK} from '../Action/Action';

class Input extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            taskText: "",
            todoTaskDisplayText: false,
            completedTaskDisplayText: false,
            displayDoneButton: false,
            duplicate: false,
            maxlength: false
        }
        this.inputRef = React.createRef(null);
        this.taskArr = [];
        this.completedTaskArr = [];
        this.permanentCompletedTaskArr = [];
    }



    addTasks = (event) => {
        event.preventDefault();
        let duplicateTask = this.noDuplicateTask(this.state.taskText);
        if(!duplicateTask){
            this.taskArr.push(this.state.taskText);
            console.log("this props is connected to store?",this.props)
            this.setState({
                taskText: "",
                maxlength: false
            })
            this.props.dispatch({type:ADDTODOTASK})
        }
        

    }

    handleTaskText = (event) => {
        if(event.target.value.length > 20){
            this.setState({
                maxlength: true
            })
        }
        else{
            this.setState({
                taskText: event.target.value,
                duplicate: false,
                maxlength: false
            })
        }       
        
    }

    completedTask = (event) => {
        if(event.target.checked){
            this.completedTaskArr.push(event.target.parentElement.innerText);
            this.setState({
                displayDoneButton: true
            })
        }
        if(!event.target.checked){
            console.log("inside not checked condition!")
            this.completedTaskArr = this.completedTaskArr.filter((wrongClickText,index) => {
                return wrongClickText != event.target.parentElement.innerText; 
            })
            this.setState({
                displayDoneButton: false
            })
        }
        console.log("completed Task Array",this.completedTaskArr)
        // this.forceUpdate();
        
    }

    markTaskComplete = (event) => {
        this.props.dispatch({type:ADDCOMPLETEDTASK});
        this.removeTasksFromTaskArr();
        this.moveTaskToPermanentCompleted()
        // this.forceUpdate();
        this.completedTaskArr = [];
        this.setState({
            displayDoneButton: false
        });
        
    }

    moveTaskToPermanentCompleted(){
        if(!this.permanentCompletedTaskArr.length){
            this.permanentCompletedTaskArr = this.completedTaskArr.map((task) => task);
        }
        else {
            this.completedTaskArr.map((task) => {
                this.permanentCompletedTaskArr.push(task);
            });
        }
        
        // console.log("result in permanent arr",this.permanentCompletedTaskArr);
    }

    removeTasksFromTaskArr(){
        this.taskArr =this.taskArr.filter((taskName,index) => {
            if(!this.completedTaskArr.includes(taskName)){
                return taskName;
            }
        })
        
    }

    noDuplicateTask(taskName){
        console.log("inside duplicate task")
        if(this.taskArr.includes(taskName)){
            this.setState({
                duplicate: true
            })
            return true;
        }
        return false;
    }

    sortByAlphabets = (event) => {
        this.taskArr.sort();
        if(this.permanentCompletedTaskArr.length){
            this.permanentCompletedTaskArr.sort();
        }
        this.forceUpdate();
    }

    render() {
        let renderTodoText, renderCompletedTodoText, addDoneButton, duplicateMsg, maxlengthMsg;
        console.log("value of todo display text",this.props.todoTaskDisplayText);
        if( this.props.todoTaskDisplayText && this.taskArr.length){
            renderTodoText = this.taskArr.map((text,index) => {
                return <TodoPage textProps={text} completedTaskProps={this.completedTask} key={index} />
            })
        }

        if(this.state.addDoneButton || this.completedTaskArr.length){
            addDoneButton = <button className="btn btn-primary done-button" onClick={this.markTaskComplete}> Done </button>
        }

        if(this.props.completedTaskDisplayText && this.permanentCompletedTaskArr.length){
            renderCompletedTodoText = this.permanentCompletedTaskArr.map((text,index) => {
                return <CompletedTodoPage textProps={text} key={index} />
            })
        }

        if(this.state.duplicate){
            duplicateMsg = <span> Task Already Added! </span>
        }

        if(this.state.maxlength){
            maxlengthMsg = <span> Must be 20 characters or less. </span>
        }

        return (
            <div className="input-group mb-3 container common-font">
                    {/* <img src="https://ucffcb14324aebc543b317cefcda.previews.dropboxusercontent.com/p/thumb/ABJNzkq0WbZGSKW7KpqN5yu4JOKrx2zrK8pVu35h0R1kat5vEXEvVvHfF8sXx7kigG6lAXa5HrGwL5cozF9DKmFBdbTbTdgMIKww7K4w19JJmQz5Zglwgjim9VrzOAiaidWRvBGY_QR_YVIbeJelEKxCH7eQtKLNrAEgeJPsi0D3RgWibeutzKLdXyzIMb6erpORiSxIuDk_F2PDRClakcb1QsSKBN8_Gj8FLNSVhJBn0cudO-13V-1NJOtzP3G96NcxZAbe_tUmyp685Wk0bm9vLBf3LNzttSRF68EPtBNKxYBnW4cEhpFSKoC-DkPmfrwv9d51i812HNI2l4CRPPOF5e8tmwv5Vj-QW_0_GiUG9A/p.png?size=178x178&size_mode=1" height="20px" /> */}
                    <input 
                    type="text" 
                    className="form-control custom-input" 
                    placeholder="Add a task"
                    value = {this.state.taskText}
                    onChange = {this.handleTaskText}
                    ref={this.inputRef}
                    />
                    <button className="btn btn-primary add-button" onClick={this.addTasks} >
                        Add
                    </button>
                    <button className="btn btn-primary sort-button" onClick={this.sortByAlphabets} >
                        Sort
                    </button>
                    <div className="container danger">
                        {duplicateMsg}
                    </div>
                    <div className="container danger">
                        {maxlengthMsg}
                    </div>
                    <div className="container no-padding d-flex">
                        <div className="col-6">
                            <div className="todo-heading" >
                                To-do
                            </div>
                        {renderTodoText}
                        <div className="done-button-position">
                            {addDoneButton}
                        </div>
                       </div>
                        <div className="col-6">
                            {/*completed task html here*/}
                            <div className="todo-heading " >
                                Completed
                            </div>
                            {renderCompletedTodoText}
                        </div>


                    </div>
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(
    mapStateToProps
    )(Input)
