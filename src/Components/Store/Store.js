const { createStore } = require('redux');
import reducer from '../Reducer/Reducer';

const store = createStore(reducer,{
    todoTaskDisplayText: false,
    completedTaskDisplayText: false
})

export default store