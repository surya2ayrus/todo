import {ADDTODOTASK, ADDCOMPLETEDTASK} from '../Action/Action';

const reducer = function (state,action){
    console.log("inside reducer");
    switch(action.type){
        case ADDTODOTASK:
            return {...state, todoTaskDisplayText: true}
        case ADDCOMPLETEDTASK:
            return {...state, completedTaskDisplayText:true}
        default:
            return state
    }
}
export default reducer;