import React, { Component } from 'react';
import '../../../css/todo.css'

class CompletedTodoPage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        return (
            <div className="col-6" >
                {/* <img src="file:///home/sift/todo-task/images/not-checked.png" /> */}
                <li className="list-group-item no-border strike">
                    <input className="form-check-input me-1" type="checkbox" defaultChecked="true" value="" />
                    {this.props.textProps}
                </li>
            </div>
        )
    }
}

export default CompletedTodoPage
