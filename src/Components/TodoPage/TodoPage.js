import React, { Component } from 'react'
import '../../../css/todo.css'

class TodoPage extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    testfunc = (event) => {
        console.log("event",event.target.parentElement.innerText)
    }

    render() {
        return (
            <div className="col-6 text-font" >
                {/* <img src="file:///home/sift/todo-task/images/not-checked.png" /> */}
                <li className="list-group-item no-border">
                    <input className="form-check-input me-1" type="checkbox" value="" onClick={this.props.completedTaskProps} />
                    {this.props.textProps}
                </li>
            </div>
        )
    }
}

export default TodoPage
